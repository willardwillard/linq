﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DSandLINQ
{
    class TaskState
    {
        public int Id { get; set; }
        public string Value { get; set; }
    }
}
