﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DSandLINQ
{
    class ProjectTask
    {
        
        public int Id { get; set; }
        public string Name { get; set; }
        public string Desc { get; set; }
        public DateTime Created { get; set; }
        public DateTime Finished { get; set; }
        public int State { get; set; }
        public int ProjectId { get; set; }
        public int PerformerId { get; set; }
    }
}
