﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DSandLINQ
{
    class Project
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Desc { get; set; }
        public DateTime Created { get; set; }
        public DateTime DeadLine { get; set; }
        public User AuthorId { get; set; }
        public Team TeamId { get; set; }
        public virtual IEnumerable<ProjectTask> Tasks { get; set; }
    }
}
