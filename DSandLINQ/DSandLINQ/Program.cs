﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using System.Net.Http;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DSandLINQ
{
    class Program
    {
        static void Main(string[] args)
        {
            bool DataFetched = false;
            IEnumerable<Project> projectList = null;

            while (!DataFetched)
            {
                Console.WriteLine("Hello, welcome to my DataStructureAndLinq Program."+"\n"+ 
                    "Structure of data is Project->Tasks->Performer,Author,Team" +"\n" +
               "If you want to start press Enter");
                Console.ReadLine();

                projectList = GetProjectStructure();
                if (projectList != null)
                {
                    DataFetched = true;
                    Console.WriteLine("Data is successfully fetched, let's continue");
                    Console.ReadLine();
                }
            }
            bool IsWork = true;

            Console.WriteLine("Okey, let's process with data");
            while (IsWork)
            {
                Console.WriteLine(
                    "Please chose the option from the list above!" + "\n" +
                    "1) Get the number of tasks in the projects of a specific user (by identifier) " +"\n" +
                    "(dictionary, where the key is the project id, and the value of the number of tasks)" + "\n" +
                    "2) Get a list of user - specific tasks(by id)" +"\n"+
                    "where the name is < 45 - character task(collection of tasks)." +"\n"+
                    "5) Get list of projects alphabetically (ascending) with sorted tasks items by length name (descending)" +"\n"+
                    "8) Exit." + "\n" );

                int choose;
                try
                {
                    choose = Convert.ToInt32(Console.ReadLine());
                }
                catch
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("------------------------" + "\n" +
                        "Please enter valid input" +
                        "\n" + "---------------------------");
                    Console.ResetColor();
                    continue;
                }
                switch (choose)
                {
                    case 2:
                        Console.WriteLine("You choose option " + "\n" +
                            "2) Get a list of user - specific tasks(by id)" +
                            "where the name is < 45 - character task(collection of tasks)." + "\n" +
                            "Please enter userId");
                        int userId2;
                        try
                        {
                            userId2 = Convert.ToInt32(Console.ReadLine());
                        }
                        catch
                        {
                            Console.WriteLine("------------------------" + "\n" +
                                "Please enter valid input(integer)" +
                                "\n" + "---------------------------");
                            break;
                        }
                        var tasks = GetTasksByLength(projectList, userId2);
                        if (tasks != null)
                        {
                            foreach (ProjectTask c in tasks)
                            {
                                Console.WriteLine("Id:" + c.Id + "\n" + c.Created + "\n"
                                    + "Name:" + c.Name + "\n" + "Finished:" + c.Finished + "\n");
                            }
                            Console.WriteLine("---------------------------" + "\n");
                        }
                        break;
                    case 1:
                        Console.WriteLine("You choose option " + "\n" +
                            "1) Get the number of tiles in the projects of a specific user (by identifier) " +
                            "​​(dictionary, where the key is the project, and the value of the number of drawers)");
                        int userId1;
                        try
                        {
                            userId1 = Convert.ToInt32(Console.ReadLine());
                        }
                        catch
                        {
                            Console.WriteLine("------------------------" + "\n" +
                                "You have char in input instead of int. Please enter valid input" +
                                "\n" + "---------------------------");
                            break;
                        }

                        var getCount = GetNumberOfTasks(projectList, userId1);
                        if (getCount != null)
                        {
                            Console.WriteLine("Done todos: " + "\n");

                            foreach (KeyValuePair<int, int> gt in getCount)
                            {
                                Console.WriteLine("Id:  " + gt.Key + "    name: " + gt.Value + "\n");
                            }
                            Console.WriteLine("---------------------------" + "\n");
                        }
                        break;
                    case 5:
                        Console.WriteLine("You choose option " + "\n" +
                            "5) Get list of projects alphabetically (ascending) with sorted tasks items by length name (descending)" + "\n");
                        var userList = GetSortedProjects(projectList);
                        if (userList != null)
                        {
                            Console.WriteLine("Sorted users list with sorted todos: " + "\n");

                            foreach (Project p in userList)
                            {
                                Console.WriteLine("ProjectName:  " + p.Name + "\n");
                                if (p.Tasks.Count() != 0)
                                {
                                    Console.WriteLine("Tasks: " + "\n");
                                    foreach (var t in p.Tasks)
                                    {
                                        Console.WriteLine("* " + t.Name + "\n");
                                    }
                                }
                                Console.WriteLine("--------------" + "\n");
                            }

                        }
                        break;
                    case 8:
                        IsWork = false;
                        break;
                    default:
                        Console.WriteLine("Sorry, there is no such option. Please choose another!");
                        break;
                }
            }




        }
        public static IEnumerable<ProjectTask> GetTasksByLength(IEnumerable<Project> projects, int userId)
        {
            var user = projects.Where(u => u.AuthorId.Id == userId).FirstOrDefault();
            if (user != null)
            {
                var tasks = projects.SelectMany(p => p.Tasks).Where(c => c.Name.Length < 45).ToList();
                if (tasks == null)
                {
                    Console.WriteLine("There is no comments with this" +
                        " requirements for selected user " + "\n" +
                        "---------------------------" + "\n");
                    return null;
                }
                return tasks;
            }
            Console.WriteLine("There is no such user" + "\n" +
                    "---------------------------" + "\n");
            return null;
        }
       
        public static Dictionary<int,int> GetNumberOfTasks(IEnumerable<Project> projects, int userId)
        {
            var user = projects.Where(u => u.Id == userId).FirstOrDefault();
            
            if(user !=null)
            {
                var selected = projects.Where(u => u.AuthorId.Id == user.Id)
                    .Select(res => new { Id = res.Id, Count = res.Tasks.Count() })
                    .ToDictionary(t => t.Id, t => t.Count);
                if (selected.Count() == 0)
                {
                    Console.WriteLine("There is no done todos for selected user" + "\n" +
                    "---------------------------" + "\n");
                    return null;
                }
                return selected;
            }
            Console.WriteLine("There is no such user" + "\n" +
                    "---------------------------" + "\n");
            return null;
        }
        public static IEnumerable<Project> GetSortedProjects(IEnumerable<Project> projects)
        {
            if (projects == null)
            {
                Console.WriteLine("No projects in list" + "\n" +
                    "---------------------------" + "\n");
                return null;
            }

            var res = projects
            .OrderBy(p => p.AuthorId.FirstName)
            .Select(p => new Project
            {
                Id = p.Id,
                Tasks = p.Tasks.Select(x => x).OrderByDescending(task => task.Name).ToList(),
                Name = p.Name
                
            }).ToList();
            return res;
        }
        public static IEnumerable<Project> GetProjectStructure()
        {
            HttpClient client = new HttpClient();

            List<ProjectModel> projects = null;
            List<TaskModel> tasks = null;
            List<TaskStateModel> taskStates = null;
            List<TeamModel> teams = null;
            List<UserModel> users = null;

            #region ProjectsResponse
            var projectsResponse = client.GetAsync("https://bsa2019.azurewebsites.net/api/Projects")
                .ContinueWith((taskwithresponse) =>
                {
                    var resp = taskwithresponse.Result;
                    var jsonString = resp.Content.ReadAsStringAsync();
                    jsonString.Wait();
                    projects = JsonConvert.DeserializeObject<List<ProjectModel>>(jsonString.Result);

                });
            projectsResponse.Wait();
            #endregion
            #region TasksResponse
            var tasksResponse = client.GetAsync("https://bsa2019.azurewebsites.net/api/Tasks")
                .ContinueWith((taskwithresponse) =>
                {
                    var resp = taskwithresponse.Result;
                    var jsonString = resp.Content.ReadAsStringAsync();
                    jsonString.Wait();
                    tasks = JsonConvert.DeserializeObject<List<TaskModel>>(jsonString.Result);

                });
            tasksResponse.Wait();
            #endregion
            #region TeamsResponse
            var teamsResponse = client.GetAsync("https://bsa2019.azurewebsites.net/api/Teams")
                .ContinueWith((taskwithresponse) =>
                {
                    var resp = taskwithresponse.Result;
                    var jsonString = resp.Content.ReadAsStringAsync();
                    jsonString.Wait();
                    teams = JsonConvert.DeserializeObject<List<TeamModel>>(jsonString.Result);

                });
            teamsResponse.Wait();
            #endregion
            #region UsersResponse
            var usersResponse = client.GetAsync("https://bsa2019.azurewebsites.net/api/Users")
                .ContinueWith((taskwithresponse) =>
                {
                    var resp = taskwithresponse.Result;
                    var jsonString = resp.Content.ReadAsStringAsync();
                    jsonString.Wait();
                    users = JsonConvert.DeserializeObject<List<UserModel>>(jsonString.Result);

                });
            usersResponse.Wait();
            #endregion
            #region TaskStatesResponse
            var taskStatesResponse = client.GetAsync("https://bsa2019.azurewebsites.net/api/TaskStates")
                .ContinueWith((taskwithresponse) =>
                {
                    var resp = taskwithresponse.Result;
                    var jsonString = resp.Content.ReadAsStringAsync();
                    jsonString.Wait();
                    taskStates = JsonConvert.DeserializeObject<List<TaskStateModel>>(jsonString.Result);

                });
            taskStatesResponse.Wait();
            #endregion
            if (projects == null || tasks == null || taskStates == null || teams == null || users == null)
            {
                Console.WriteLine("There were some errors while fetching data");
                return null;
            }
            var MyStructure = projects.Select(project => new Project
            {
                Id = project.Id,
                Name = project.Name,
                Desc = project.Desc,
                Created = project.Created,
                DeadLine = project.DeadLine,
                TeamId = teams.Where(team => team.Id == project.TeamId)
                                    .Select(t => new Team
                                    {
                                        Id = t.Id,
                                        Name = t.Name,
                                        Created = t.Created
                                    }).FirstOrDefault(),
                AuthorId = users.Where(user => user.Id == project.AuthorId)
                                    .Select(u => new User
                                    {
                                        Id = u.Id,
                                        FirstName = u.FirstName,
                                        LastName = u.LastName,
                                        Email = u.Email,
                                        Birthday = u.Birthday,
                                        Registered = u.Registered,
                                        TeamId = u.TeamId
                                    }).FirstOrDefault(),
                Tasks = tasks.Where(task => task.ProjectId == project.Id)
                                    .Select(t => new ProjectTask
                                    {
                                        Id = t.Id,
                                        Name = t.Name,
                                        Desc = t.Desc,
                                        Created = t.Created,
                                        Finished = t.Finished,
                                        
                                        PerformerId = t.PerformerId,
                                        ProjectId = t.ProjectId,
                                        State = t.State
                                    }).ToList()
            }).ToList();
          
            return MyStructure;

        }
    }
}
